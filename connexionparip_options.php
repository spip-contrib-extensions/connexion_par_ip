<?php
/**
 * Fichier d'options
 *
 * @plugin     connexionparip
 * @copyright  2021
 * @author     chankalan
 * @licence    GNU/GPL
 * @package    SPIP\Connexionparip\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



function connexionparip_affichage_final($page){

	// sans session existante on va identifier l'auteur suivant son IP et le loger
	if (!isset($_COOKIE['spip_session'])) {
		
		$ip = $GLOBALS['ip'];
		$ipset = ipset_get();
		if ($ipset->match($ip)) {
			// On connait cette IP
			$id_auteur = ipset_access_author($ip);
			$auteur = sql_fetsel("*","spip_auteurs","id_auteur=$id_auteur");
			// alors on connecte
			include_spip('inc/auth');
			auth_loger($auteur);
		}
	}
	return $page;
}

